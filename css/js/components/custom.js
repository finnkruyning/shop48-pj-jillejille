$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
        $('body').find('.js-hide-on-logged-in').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
        $('body').find('.js-hide-on-logged-out').remove();
    }



    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header
});

