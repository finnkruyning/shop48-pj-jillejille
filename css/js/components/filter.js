$(function () {

    var $accordion = $('.tag-group-ul');

    $accordion.each(function () {
        var _self = $(this);
        var openAccordion = _self.find('h3');
        var accordionBody = _self.find('ul');
        accordionBody.addClass('show');
        $('<i class="icon"></i>').appendTo(openAccordion);

        if (accordionBody.hasClass('show')) {
            _self.find('h3').addClass('active');
            openAccordion.find('.icon').addClass('icon-chevron-down');
        } else {
            _self.find('h3').removeClass('active');
            openAccordion.find('.icon').addClass('icon-chevron-right');
        }

    });

    $accordion.find('h3').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($accordion).find('ul');
        var icon = _self.find('.icon');

        target.toggleClass('show');
        
        if(target.is(":visible")){
            _self.addClass('active');
            icon.removeClass('icon-chevron-right').addClass('icon-chevron-down');
        } else {
            _self.removeClass('active');
            icon.removeClass('icon-chevron-down').addClass('icon-chevron-right');
        }
        
    });

});
