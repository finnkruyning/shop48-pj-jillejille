jQuery(function(){
	jQuery('#body_basket div#rebate div.button').addClass('tit-rebate');
	jQuery('#body_basket div#rebate div.tit-rebate').removeClass('button');
	jQuery('#body_basket div#rebate div.tit-rebate').attr('style','');
	jQuery('#body_basket div#rebate div.tit-rebate').text('Kortingscode:');
	jQuery('#body_basket div#rebate div.tit-rebate').next().addClass('cont-rebate');
	jQuery('#body_basket div#rebate input[type="submit"]').attr('value','Toevoegen'); 

	jQuery('#body_addresses .address table tr:last-child').find('span#new').parents('tr').addClass('row-last');
	jQuery('#body_addresses .address table tr.row-last td:eq(1)').remove();
	jQuery('#body_addresses .address table tr.row-last td:eq(1)').attr('colspan','3'); 

	jQuery('#body_address .address table tbody tr:eq(1)').remove();
	jQuery('#body_address .address table tbody tr:eq(1)').remove(); 
	
	jQuery('#body_address .address table td #address_last_name').parent().prev().addClass('pad');
	jQuery('#body_address .address table td #address_suffix').parent().prev().addClass('pad');
	jQuery('#body_address .address table td #address_city').parent().prev().addClass('pad'); 
	
	jQuery("#body_basket #basket tr td").each(function(){
		jQuery(this).find("div").addClass("style");
	});
	
	jQuery('#basket tr:eq(0)').addClass('row-first');
	jQuery('#basket tr').attr('style','');
	jQuery('#basket tr td').each(function(){
		var style = jQuery(this).find('div').attr('style');
		if (style == "float:right;") {
			jQuery(this).find('div').addClass('price');
		}
	}); 
	
	jQuery('#basket td div').each(function() {
		var style=jQuery(this).attr('style');
		if (style='float:right;') {
			jQuery(this).addClass('price');
		} 
		jQuery('#nieuwsbrief').parent().parent().addClass('block-form');
	});
});
